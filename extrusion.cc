#include <iostream>  // for input output
#include <fstream>   // for file I/O
#include <cassert>   // for assert function
#include <cstdlib>   // for EXIT_FAILURE
#include <vector>    // for vector

#include "grid.h"
#include "topo.h"

using namespace std;

//------------------------------------------------------------------------------
// This function extrudes 2D grid
// 1) Adds displacements to each vertex
// 2) converts faces into cells
// 3) converts edges into boundary faces
//------------------------------------------------------------------------------
void Grid::extrusion()
{
   double start, finish;
   start  = clock();
   cout << "Extruding 2D mesh " << endl;

   unsigned int t, n;

   Topo topo;

   // allocate size
   for(unsigned int i=0; i<nL+1; i++) layer[i].vertex.resize(nv);
   for(unsigned int i=0; i<nL; i++)   layer[i].cell.resize(nbf);
   for(unsigned int i=0; i<nL; i++)   layer[i].face.resize(nbe);

   // form vertices
   for(unsigned int i=0; i<nv; i++) layer[0].vertex[i] = vertex[i]; 

   for(unsigned int i=1; i<nL+1; i++)
   {
      for(unsigned int j=0; j<nv; j++) layer[i].vertex[j] = layer[i-1].vertex[j] + layer[i-1].extr;
   }

   // form cells from faces
   for(unsigned int i=0; i<nbf; i++)  // form first layer
   {
       t = face[i].gmshtype;
       n = topo.nve(t);
       if(t==2) layer[0].cell[i].gmshtype = 6;
       if(t==3) layer[0].cell[i].gmshtype = 5;

       for(unsigned int j=0; j<face[i].ntags; j++) layer[0].cell[i].tag[j] = face[i].tag[0];

       for(unsigned int j=0; j<n; j++) layer[0].cell[i].vertex[j]   = face[i].vertex[j];
       for(unsigned int j=0; j<n; j++) layer[0].cell[i].vertex[n+j] = face[i].vertex[j]+nv;
   }
   for(unsigned int i=1; i<nL; i++) // form remaining layers
   {
       for(unsigned int j=0; j<nbf; j++)
       { 
           t = face[j].gmshtype;
           n = topo.nve(t);
           if(t==2) layer[i].cell[j].gmshtype = 6;
           if(t==3) layer[i].cell[j].gmshtype = 5;

           for(unsigned int k=0; k<face[k].ntags; k++) layer[i].cell[j].tag[k] = face[j].tag[0];

           for(unsigned int k=0; k<n; k++) layer[i].cell[j].vertex[k]   = layer[i-1].cell[j].vertex[n+k];
           for(unsigned int k=0; k<n; k++) layer[i].cell[j].vertex[n+k] = layer[i-1].cell[j].vertex[n+k]+nv;
       }
   }

   // form boundary faces from edges
   for(unsigned int i=0; i<nbe; i++)  // form first layer
   {
        t = edge[i].gmshtype;
        n = topo.nve(t);
        layer[0].face[i].gmshtype = 3;

        for(unsigned int j=0; j<edge[i].ntags; j++) layer[0].face[i].tag[j] = edge[i].tag[0];

        layer[0].face[i].vertex[0] = edge[i].vertex[0];
        layer[0].face[i].vertex[1] = edge[i].vertex[1];
        layer[0].face[i].vertex[2] = edge[i].vertex[1]+nv;
        layer[0].face[i].vertex[3] = edge[i].vertex[0]+nv;
   }
   for(unsigned int i=1; i<nL; i++)  // form remaining layers
   {
       for(unsigned int j=0; j<nbe; j++)
       {
           t = edge[j].gmshtype;
           n = topo.nve(t);
           layer[i].face[j].gmshtype = 3;

           for(unsigned int k=0; k<edge[j].ntags; k++) layer[i].face[j].tag[k] = edge[j].tag[0];

           layer[i].face[j].vertex[0] = layer[i-1].face[j].vertex[2];
           layer[i].face[j].vertex[1] = layer[i-1].face[j].vertex[3];
           layer[i].face[j].vertex[2] = layer[i-1].face[j].vertex[3]+nv;
           layer[i].face[j].vertex[3] = layer[i-1].face[j].vertex[2]+nv;
       }
   }

   finish = clock();

   cout << "Time required = " << (finish-start)/ (double) CLOCKS_PER_SEC << " seconds" << endl;
   cout << "=======================================================" << endl;
}
