#ifndef __TOPO_H__
#define __TOPO_H__

class Topo
{
public:
   Topo () { };  

   unsigned int nve(unsigned int); 
   unsigned int neve(unsigned int, unsigned int);
   unsigned int connect(unsigned int, unsigned int, unsigned int);
   unsigned int nee(unsigned int);
};

#endif
