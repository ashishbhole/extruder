lx = 1.0;
ly = 1.0;

Point(1) = {0 , 0, 0};
Point(2) = {lx, 0, 0};
Point(3) = {lx,ly, 0};
Point(4) = {0 ,ly, 0};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

Line Loop(1) = {1,2,3,4};
Plane Surface(1) = {1};

Transfinite Line {1,2,-3,-4} = 10; 
//Transfinite Surface "*";
//Recombine Surface "*";

Mesh.Algorithm = 1;

Physical Line(1) = {1};
Physical Line(2) = {2};
Physical Line(3) = {3};
Physical Line(4) = {4};
Physical Surface(10) = {1};
