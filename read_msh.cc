#include <iostream>  // for input output
#include <fstream>   // for file I/O
#include <cassert>   // for assert function
#include <string>    // for string
#include <cstdlib>   // for EXIT_FAILURE
#include <vector>    // for vector
#include <cstring>   // for strcpy, strcat

#include "grid.h"
#include "topo.h"

using namespace std;

//------------------------------------------------------------------------------
// Read 2D grid in msh format
// This function reads following information
// 1) cordinates of vertices
// 2) elements (faces and boundary edges) with their connectivity
//------------------------------------------------------------------------------
void Grid::read_msh()
{
   double start, finish;
   start  = clock();
   cout << "Reading 2D grid from msh file " << endl;

   unsigned int coun;
   char filen[96];

   ntri = nquad = 0;

   Topo topo;

   ifstream file;
   strcpy(filen, filename.c_str());
   file.open (filen);
   assert (file.is_open());

   string line;
   file >> line;
   while (line != "$Nodes")
          file >> line;

   // Read vertices
   file >> nv;
   assert (nv > 0);
   vertex.resize(nv);
   for(unsigned int i=0; i<nv; ++i) file >> coun >> vertex[i].x >> vertex[i].y >> vertex[i].z;

   file >> line;
   while(line != "$Elements") file >> line;
   
   // Read elements
   file >> nelem;
   assert (nelem > 0);
   elem.resize(nelem);

   for(unsigned int i=0; i<nelem; i++)
   {
       file >> coun >> elem[i].gmshtype >>  elem[i].ntags; 
       assert(elem[i].ntags==2 || elem[i].ntags==3 );

       // boundary edges
       if(elem[i].gmshtype == 1)
       {
           edge.resize(nbe+1);
           edge[nbe].gmshtype = elem[i].gmshtype;
           edge[nbe].ntags    = elem[i].ntags;
           for(unsigned int p = 0; p<edge[nbe].ntags; p++) file >> edge[nbe].tag[p];

           for(unsigned int p = 0; p<topo.nve(edge[nbe].gmshtype); p++)
           {
               // store vertices and substract 1 from them to start indexing from 0
               file >> edge[nbe].vertex[p];                   
               edge[nbe].vertex[p] =  edge[nbe].vertex[p] - 1;
           }

           ++nbe;
      }
      else if((elem[i].gmshtype == 2) || (elem[i].gmshtype == 3)) // tris and quads
      {
           face.resize(nbf+1);
           face[nbf].gmshtype = elem[i].gmshtype;
           face[nbf].ntags    = elem[i].ntags;
           for(unsigned int p = 0; p<face[nbf].ntags; p++) file >> face[nbf].tag[p];

           for(unsigned int p = 0; p<topo.nve(face[nbf].gmshtype); p++)
           {   
               // store vertices and substract 1 from them to start indexing from 0
               file >> face[nbf].vertex[p];               
               face[nbf].vertex[p] =  face[nbf].vertex[p] - 1;
           }

           if(face[nbf].gmshtype == 2) ++ntri;
           else ++nquad;

           ++nbf;
       }
      else
      {
           cout << "Error: detected unknown element type !!!" << endl;
           cout << "Element type =" << elem[i].gmshtype << endl;
           exit (EXIT_FAILURE);
      }
   }

   printf("#Vertices       : %d\n", nv);
   printf("#Triangles      : %d\n", ntri);
   printf("#Quadrangle     : %d\n", nquad);
   printf("Total #edges    : %d\n", nbe);
   printf("Total #faces    : %d\n", nbf);

   elem.clear();
  
   file.close ();

   finish = clock();
   cout << "Time required for reading msh file = " << (finish-start)/ (double) CLOCKS_PER_SEC << " seconds" << endl;
   cout << "=======================================================" << endl;
}
