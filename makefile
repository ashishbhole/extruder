# Set the C compiler
CC     = g++
CCFLAG = -Wall -O3
    
HDR = $(wildcard *.h)
SRC = $(wildcard *.cc)
OBJS = $(patsubst %.cc,%.o,$(SRC))

MAKEFILE = makefile

PROGRAM  = extrude

all:     $(PROGRAM)

.cc.o: $(HDRS)
	$(CC) $(CCFLAG) -c $*.cc

$(PROGRAM): $(OBJS)
	$(CC) -o $(PROGRAM) $(OBJS)

clean: 
	rm -f $(OBJS) $(PROGRAM)

