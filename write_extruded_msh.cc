#include <iostream>  // for input output
#include <fstream>   // for file I/O
#include <cassert>   // for assert function
#include <cstdlib>   // for EXIT_FAILURE
#include <vector>    // for vector
#include <cstring>

#include "grid.h"
#include "topo.h"
using namespace std;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void Grid::write_extruded_msh()
{
   double start, finish;
   start  = clock();
   cout << "Writing extruded mesh to file " << endl;

   unsigned int nf, ncells, coun;
   char filen[96];

   Topo topo;

   filename.erase(filename.length()-4);
   strcpy(filen, filename.c_str());
   strcat(filen, "_extruded.msh");

   ofstream file;
   file.open (filen, ios::out);
   assert (file.is_open());

   file << "$MeshFormat" << endl;
   file << "2.2 0 8" << endl;
   file << "$EndMeshFormat" << endl;

   file << "$Nodes" << endl;
   file << nv*(nL+1) << endl;
   for(unsigned int j=0; j<nL+1; j++)
   {
      for(unsigned int i=0; i<nv; i++)
      {
         file << (nv*j)+i+1 << " " << layer[j].vertex[i].x << " " << layer[j].vertex[i].y << " " << layer[j].vertex[i].z << endl;
      }
   }
   file << "$EndNodes" << endl;

   nf = 2*nbf + nbe*nL;
   ncells = nbf*nL;

   file << "$Elements" << endl;
   file << nf+ncells << endl;
   coun = 0;
   // write boundary faces
   for(unsigned int i=0; i<nL; i++)
   {
      for(unsigned int j=0; j<nbe; j++)
      {
          coun++;
          file << coun << " "  << layer[i].face[j].gmshtype << " " << edge[i].ntags << " ";
          for(unsigned int p=0; p<edge[j].ntags; p++) file << layer[i].face[j].tag[p] << " ";

          for(unsigned int p=0; p<topo.nve(layer[i].face[j].gmshtype); p++) file << layer[i].face[j].vertex[p]+1 << " ";

          file << endl;
      }
   }
   for(unsigned int i=0; i<nbf; i++)
   {
       coun++;
       file << coun << " "  << face[i].gmshtype << " " << face[i].ntags << " ";
       for(unsigned int p=0; p<face[i].ntags; p++) file << 301 << " ";

       for(unsigned int p=0; p<topo.nve(face[i].gmshtype); p++) file << face[i].vertex[p]+1 << " ";

       file << endl;
   }
   for(unsigned int i=0; i<nbf; i++)
   {
       coun++;
       file << coun << " "  << face[i].gmshtype << " " << face[i].ntags << " ";
       for(unsigned int p=0; p<face[i].ntags; p++) file << 302 << " ";

       for(unsigned int p=0; p<topo.nve(face[i].gmshtype); p++) file << face[i].vertex[p]+1+(nv*nL) << " ";

       file << endl;
   }
   // write cells
   for(unsigned int i=0; i<nL; i++)
   {
      for(unsigned int j=0; j<nbf; j++)
      {
          coun++;
          file << coun << " "  << layer[i].cell[j].gmshtype << " " << face[j].ntags << " ";
          for(unsigned int p=0; p<face[j].ntags; p++) file << face[j].tag[p] << " ";

          for(unsigned int p=0; p<topo.nve(layer[i].cell[j].gmshtype); p++) file << layer[i].cell[j].vertex[p]+1 << " ";

          file << endl;
      }
   }
   file << "$EndElements" << endl;

   file << "$Periodic" << endl;
   file << np_ent << endl;
   for(unsigned int i=0; i<np_ent; i++)
   {
      file << 2 << " " << slave_tag[i] << " " << master_tag[i] << endl;
      file << "Affine 1 0 0 -1 0 1 0 0 0 0 1 0 0 0 0 1" << endl;
      file << npairs[i] << endl;
      for(unsigned int j=0; j<npairs[i]; j++) file << perv[i].slave[j] << " " << perv[i].master[j] << endl;
   }
   file << "$EndPeriodic" << endl;

   file.close ();

   finish = clock();
   cout << "Time required = " << (finish-start)/ (double) CLOCKS_PER_SEC << " seconds" << endl;
   cout << "=======================================================" << endl;
}
