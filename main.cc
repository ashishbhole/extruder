#include <iostream>  // for input output
#include <fstream>   // for file I/O
#include <time.h>    // for start and end time
#include <cassert>   // for assert function
#include <cstdlib>   // for EXIT_FAILURE
#include <string>

#include "grid.h"

using namespace std;

int main(int argc, char *argv[]) 
{
    double start, finish;
    start  = clock();
    string extension;

    Grid grid;

    assert(argc==4);

    // input filename without extension
    grid.filename = argv[1];
    extension = grid.filename.substr(grid.filename.length() - 3);

    // input no. of layers for which 2D grid is to be extruded
    grid.nL = atoi(argv[2]);
    assert(grid.nL>0);

    grid.layer.resize(grid.nL+1);

    // form a vector for extrusion
    for(unsigned int i=0; i<grid.nL; i++) grid.layer[i].extr.z = atof(argv[3]);

    if(extension == "msh") grid.read_msh(); 
    else if(extension == "ugr")grid.read_blazek_grid(); 

    // generate extruded grid from 2D grid and extrusion vector
    grid.extrusion();
    // write extruded grid
    grid.write_extruded_msh();
   
    finish = clock();
    cout << "Time required for extrusion of mesh = " << (finish-start)/ (double) CLOCKS_PER_SEC << " seconds" << endl;

    return 0;
}
