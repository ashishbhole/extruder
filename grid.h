#ifndef __GRID_H__
#define __GRID_H__

#include <vector>
#include <string>
#include "vec.h"

struct Element
{
public:
  unsigned int gmshtype, ntags;    
};

struct Edge
{
public:
  unsigned int vertex[2], tag[3], ntags, gmshtype;
};

struct Face
{
public:
  unsigned int vertex[4], tag[3], ntags, gmshtype;
};

struct Cell
{ 
public:
  unsigned int vertex[8], tag[3], ntags, gmshtype;
};

struct Periodic
{
public:
  std::vector<unsigned int> slave, master;
};

struct Layer
{
public:
  std::vector<Vector> vertex;
  Vector extr;   // extrusion vector     
  std::vector<Cell> cell;
  std::vector<Face> face; 
};

class Grid
{
public:
   Grid () { nv = nc = nelem = nbe = nbf = ntri = nquad = nL = np_ent = 0; };
   unsigned int nv, nc, nelem, nbe, nbf, ntri, nquad, nL, np_ent;
   
   std::string filename;
   std::vector<Vector>       vertex; 
   std::vector<Element>      elem;
   std::vector<Cell>         cell;
   std::vector<Face>         face;
   std::vector<Edge>         edge;
   std::vector<Periodic>     perv;
   std::vector<unsigned int> slave_tag, master_tag, npairs;
   std::vector<Layer>        layer;

   void read_msh();
   void read_blazek_grid();
   void extrusion();
   void write_extruded_msh();
};

#endif
