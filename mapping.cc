#include <iostream>
#include <fstream>
#include <cassert>
#include <string>
#include <cstdlib>
#include <vector>
#include "topo.h"

using namespace std;

// no of vertices in the element
unsigned int Topo::nve(unsigned int type)
{
  unsigned int arr [8] = {1, 2, 3, 4, 4, 8, 6, 7};
  return arr[type];
}

// number of edges in an element
unsigned int Topo::nee(unsigned int type)
{
   unsigned int arr [4] = {0, 1, 3, 4};         
   return arr[type];
}

// number of edges formed at each vertex of the cell
unsigned int Topo::neve(unsigned int n, unsigned int type)
{
   if(type==2) {unsigned int arr [3] = {2, 2, 2}; return arr[n];}              // tet
   if(type==3) {unsigned int arr [4] = {3, 3, 3, 3}; return arr[n];}  // hex
   return 0;
}

// connect: mapping for each vertex of cell to form valid edge
// this is required for forming edge list
unsigned int Topo::connect(unsigned int n, unsigned int m, unsigned int type)
{
  if(type==2)  
  {
     if(m==0) {unsigned int arr [2] = {1,2};return arr[n];}
     if(m==1) {unsigned int arr [2] = {0,2};return arr[n];}
     if(m==2) {unsigned int arr [2] = {0,1};return arr[n];}
  }

  if(type==3)  
  {
     if(m==0) {unsigned int arr [2] = {1,3};return arr[n];}
     if(m==1) {unsigned int arr [2] = {0,2};return arr[n];}
     if(m==2) {unsigned int arr [2] = {1,3};return arr[n];}
     if(m==3) {unsigned int arr [2] = {0,2};return arr[n];}
  }
  return 0;
}
